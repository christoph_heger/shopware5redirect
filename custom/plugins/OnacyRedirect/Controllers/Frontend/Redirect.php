<?php

class Shopware_Controllers_Frontend_Redirect extends Enlight_Controller_Action
{
    public function indexAction()
    {
        $this->forward('auth', 'index', 'backend');
    }
}
