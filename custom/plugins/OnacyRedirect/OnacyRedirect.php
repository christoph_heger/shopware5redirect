<?php

namespace OnacyRedirect;

use Shopware\Components\Plugin;

class OnacyRedirect extends Plugin
{
    public static function getSubscribedEvents()
    {
        return [
            'Shopware_CronJob_RefreshSeoIndex_CreateRewriteTable' => 'createOnacyRedirectTable',
            'sRewriteTable::sCreateRewriteTable::after' => 'createOnacyRedirectTable',
        ];
    }

    public function createOnacyRedirectTable()
    {
        /** @var \sRewriteTable $rewriteTableModule */
        $rewriteTableModule = Shopware()->Container()->get('modules')->sRewriteTable();
        $rewriteTableModule->sInsertUrl('sViewport=redirect', 'admin');
    }
}
